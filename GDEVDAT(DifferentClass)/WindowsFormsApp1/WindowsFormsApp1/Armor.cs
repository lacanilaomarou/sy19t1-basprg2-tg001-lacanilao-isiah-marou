﻿using mySQLApp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Armor : Form
    {
        public Armor()
        {
            InitializeComponent();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (txtItemID.Text != "")
            {
                cmd.CommandText = @"update ArmorTest set Name = @Name, Armor = @Armor, Price = @Price, Weight = @Weight, Type = @Type, Part=@Part,ArmorSet=@AS,Material=@Mat where ItemID = @ItemID";
                cmd.Parameters.AddWithValue("@ItemID", txtItemID.Text);
            }
            else
            {
                cmd.CommandText = "Insert into ArmorTest values (@Name,@Armor,@Price,@Weight,@Type,@Part,@AS,@Mat)";
            }


            cmd.Parameters.AddWithValue("@Name", txtName.Text);
            cmd.Parameters.AddWithValue("@Armor", nudArmor.Text);
            cmd.Parameters.AddWithValue("@Price", nudPrice.Text);
            cmd.Parameters.AddWithValue("@Weight", nudWeight.Text);
            cmd.Parameters.AddWithValue("@Type", cbType.Text);
            cmd.Parameters.AddWithValue("@Part", cbPart.Text);
            cmd.Parameters.AddWithValue("@AS", cbAS.Text);
            cmd.Parameters.AddWithValue("@Mat", cbMaterial.Text);

            cmd.ExecuteNonQuery();
            Helper.con.Close();

            if (txtItemID.Text != "")
            {
                MessageBox.Show("Successfully Updated!");
            }
            else
            {
                MessageBox.Show("Successfully Added!");
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Armor_Load(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;
            cmd.CommandText = "select * from ArmorTest";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);

            dgvArmor.DataSource = dt;
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;
            cmd.CommandText = "select * from ArmorTest";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);

            dgvArmor.DataSource = dt;
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            txtItemID.Text = dgvArmor.SelectedRows[0].Cells[0].Value.ToString();
            txtName.Text = dgvArmor.SelectedRows[0].Cells[1].Value.ToString();
            nudArmor.Text = dgvArmor.SelectedRows[0].Cells[2].Value.ToString();
            nudPrice.Text = dgvArmor.SelectedRows[0].Cells[3].Value.ToString();
            nudWeight.Text = dgvArmor.SelectedRows[0].Cells[4].Value.ToString();
            cbType.Text = dgvArmor.SelectedRows[0].Cells[5].Value.ToString();
            cbPart.Text = dgvArmor.SelectedRows[0].Cells[6].Value.ToString();
            cbAS.Text = dgvArmor.SelectedRows[0].Cells[7].Value.ToString();
            cbMaterial.Text = dgvArmor.SelectedRows[0].Cells[8].Value.ToString();

        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (MessageBox.Show("Are you sure you want to delete?", "Remove Row", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                cmd.CommandText = "delete from ArmorTest where ItemID = @ItemID";
                cmd.Parameters.AddWithValue("@ItemID", dgvArmor.SelectedRows[0].Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
                Helper.con.Close();
                MessageBox.Show("Row Removed", "Remove Row", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Row Not Removed", "Remove Row", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            txtItemID.Clear();
            txtName.Clear();
            nudArmor.Text = String.Empty;
            nudPrice.Text = String.Empty;
            nudWeight.Text = String.Empty;
            cbType.Text = String.Empty;
            cbPart.Text = String.Empty;
            cbAS.Text = String.Empty;
            cbMaterial.Text = String.Empty;

        }
    }
}
