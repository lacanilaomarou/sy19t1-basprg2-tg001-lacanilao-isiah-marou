﻿using mySQLApp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Weapon : Form
    {
        public Weapon()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (txtItemID.Text != "")
            {
                cmd.CommandText = @"update WeaponTest set Name = @Name, DamageRate = @DR, Price = @Price, Weight = @Weight, Type = @Type, WeaponSet=@WS, Material=@Mat where ItemID = @ItemID";
                cmd.Parameters.AddWithValue("@ItemID", txtItemID.Text);
            }
            else
            {
                cmd.CommandText = "Insert into WeaponTest values (@Name,@DR,@Price,@Weight,@Type,@WS,@Mat)";
            }


            cmd.Parameters.AddWithValue("@Name", txtName.Text);
            cmd.Parameters.AddWithValue("@DR", nudDamage.Text);
            cmd.Parameters.AddWithValue("@Price", nudPrice.Text);
            cmd.Parameters.AddWithValue("@Weight", nudWeight.Text);
            cmd.Parameters.AddWithValue("@Type", cbType.Text);
            cmd.Parameters.AddWithValue("@WS", cbWS.Text);
            cmd.Parameters.AddWithValue("@Mat", cbMat.Text);

            cmd.ExecuteNonQuery();
            Helper.con.Close();

            if (txtItemID.Text != "")
            {
                MessageBox.Show("Successfully Updated!");
            }
            else
            {
                MessageBox.Show("Successfully Added!");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            txtItemID.Text = dgvWeapon.SelectedRows[0].Cells[0].Value.ToString();
            txtName.Text = dgvWeapon.SelectedRows[0].Cells[1].Value.ToString();
            nudDamage.Text = dgvWeapon.SelectedRows[0].Cells[2].Value.ToString();
            nudPrice.Text = dgvWeapon.SelectedRows[0].Cells[3].Value.ToString();
            nudWeight.Text = dgvWeapon.SelectedRows[0].Cells[4].Value.ToString();
            cbType.Text = dgvWeapon.SelectedRows[0].Cells[5].Value.ToString();
            cbWS.Text = dgvWeapon.SelectedRows[0].Cells[6].Value.ToString();
            cbMat.Text = dgvWeapon.SelectedRows[0].Cells[7].Value.ToString();
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Weapon_Load(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;
            cmd.CommandText = "select * from WeaponTest";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);

            dgvWeapon.DataSource = dt;
            
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Helper.openconnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;
            cmd.CommandText = "select * from WeaponTest";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);

            dgvWeapon.DataSource = dt;
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            txtItemID.Clear();
            txtName.Clear();
            nudDamage.Text = String.Empty;
            nudPrice.Text = String.Empty;
            nudWeight.Text = String.Empty;
            cbType.Text = String.Empty;
            cbWS.Text = String.Empty;
            cbMat.Text = String.Empty;
        }
    }
}
