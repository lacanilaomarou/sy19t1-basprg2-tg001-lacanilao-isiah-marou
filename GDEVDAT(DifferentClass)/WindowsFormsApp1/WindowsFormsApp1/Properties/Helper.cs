﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mySQLApp
{
    public static class Helper
    {
        public static SqlConnection con;

        public static object ConfigurationManager { get; private set; }

        public static void openconnection()
        {
            con = new SqlConnection();
            con.ConnectionString = @"Server=localhost\SQLEXPRESS;Database=master;Trusted_Connection=True;";
            con.Open();
        }
    }
}