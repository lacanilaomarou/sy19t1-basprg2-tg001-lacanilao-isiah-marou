﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Spells : Form
    {
        public Spells()
        {
            InitializeComponent();
        }

        private void Spells_Load(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbType.Text == "Destruction") nudDamage.Enabled = true;
            else nudDamage.Enabled = false;
            if (cbType.Text == "Restoration") nudHeal.Enabled = true;
            else nudHeal.Enabled = false;
            if (cbType.Text == "Illusion") nudIllusion.Enabled = true;
            else nudIllusion.Enabled = false;
            if (cbType.Text == "Alteration") nudBuff.Enabled = true;
            else nudBuff.Enabled = false;
            if (cbType.Text == "Conjuration") nudSummon.Enabled = true;
            else nudSummon.Enabled = false;
        }
    }
}
