﻿namespace WindowsFormsApp1
{
    partial class Armor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvArmor = new System.Windows.Forms.DataGridView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.AddView = new System.Windows.Forms.GroupBox();
            this.cbMaterial = new System.Windows.Forms.ComboBox();
            this.cbAS = new System.Windows.Forms.ComboBox();
            this.cbPart = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.nudArmor = new System.Windows.Forms.NumericUpDown();
            this.nudWeight = new System.Windows.Forms.NumericUpDown();
            this.nudPrice = new System.Windows.Forms.NumericUpDown();
            this.btnClear = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtItemID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArmor)).BeginInit();
            this.AddView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudArmor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvArmor
            // 
            this.dgvArmor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArmor.Location = new System.Drawing.Point(51, 11);
            this.dgvArmor.Margin = new System.Windows.Forms.Padding(2);
            this.dgvArmor.Name = "dgvArmor";
            this.dgvArmor.RowHeadersWidth = 51;
            this.dgvArmor.RowTemplate.Height = 24;
            this.dgvArmor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvArmor.Size = new System.Drawing.Size(961, 247);
            this.dgvArmor.TabIndex = 0;
            this.dgvArmor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(863, 178);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(178, 34);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add/Update";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(51, 262);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(264, 24);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Edit in Add View";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(391, 262);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(178, 24);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(783, 262);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(232, 24);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // AddView
            // 
            this.AddView.Controls.Add(this.cbMaterial);
            this.AddView.Controls.Add(this.cbAS);
            this.AddView.Controls.Add(this.cbPart);
            this.AddView.Controls.Add(this.label9);
            this.AddView.Controls.Add(this.label8);
            this.AddView.Controls.Add(this.label7);
            this.AddView.Controls.Add(this.cbType);
            this.AddView.Controls.Add(this.nudArmor);
            this.AddView.Controls.Add(this.nudWeight);
            this.AddView.Controls.Add(this.nudPrice);
            this.AddView.Controls.Add(this.btnClear);
            this.AddView.Controls.Add(this.btnAdd);
            this.AddView.Controls.Add(this.label6);
            this.AddView.Controls.Add(this.txtName);
            this.AddView.Controls.Add(this.txtItemID);
            this.AddView.Controls.Add(this.label5);
            this.AddView.Controls.Add(this.label4);
            this.AddView.Controls.Add(this.label3);
            this.AddView.Controls.Add(this.label2);
            this.AddView.Controls.Add(this.label1);
            this.AddView.Location = new System.Drawing.Point(11, 291);
            this.AddView.Name = "AddView";
            this.AddView.Size = new System.Drawing.Size(1047, 217);
            this.AddView.TabIndex = 5;
            this.AddView.TabStop = false;
            this.AddView.Text = "Add VIew";
            // 
            // cbMaterial
            // 
            this.cbMaterial.FormattingEnabled = true;
            this.cbMaterial.Items.AddRange(new object[] {
            "Corundum Ingot\t",
            "Steel Ingot",
            "Leather",
            "Refined Moonstone",
            "Chitin Plate",
            "Quicksilver Ingot",
            "\tRefined Moonstone",
            "\tRefined Malachite",
            "Iron Ingot",
            "Ebony Ingot",
            "Dwarven Metal Ingot",
            "Orichalcum Ingot\t"});
            this.cbMaterial.Location = new System.Drawing.Point(844, 61);
            this.cbMaterial.Name = "cbMaterial";
            this.cbMaterial.Size = new System.Drawing.Size(193, 21);
            this.cbMaterial.TabIndex = 21;
            // 
            // cbAS
            // 
            this.cbAS.FormattingEnabled = true;
            this.cbAS.Items.AddRange(new object[] {
            "Deadric",
            "Dragon",
            "Dwarven",
            "Ebony",
            "Elven",
            "Orcish",
            "Steel",
            "Glass"});
            this.cbAS.Location = new System.Drawing.Point(844, 27);
            this.cbAS.Name = "cbAS";
            this.cbAS.Size = new System.Drawing.Size(193, 21);
            this.cbAS.TabIndex = 20;
            // 
            // cbPart
            // 
            this.cbPart.FormattingEnabled = true;
            this.cbPart.Items.AddRange(new object[] {
            "Body",
            "Boots",
            "Gauntlets",
            "Helmet",
            "Shield"});
            this.cbPart.Location = new System.Drawing.Point(548, 58);
            this.cbPart.Name = "cbPart";
            this.cbPart.Size = new System.Drawing.Size(198, 21);
            this.cbPart.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(769, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Material";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(769, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Armor Set";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(486, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Part";
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Heavy Armor",
            "Light Armor",
            "Robe/Clothes"});
            this.cbType.Location = new System.Drawing.Point(548, 28);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(198, 21);
            this.cbType.TabIndex = 15;
            // 
            // nudArmor
            // 
            this.nudArmor.Location = new System.Drawing.Point(326, 29);
            this.nudArmor.Name = "nudArmor";
            this.nudArmor.Size = new System.Drawing.Size(120, 20);
            this.nudArmor.TabIndex = 14;
            // 
            // nudWeight
            // 
            this.nudWeight.Location = new System.Drawing.Point(326, 81);
            this.nudWeight.Name = "nudWeight";
            this.nudWeight.Size = new System.Drawing.Size(120, 20);
            this.nudWeight.TabIndex = 13;
            // 
            // nudPrice
            // 
            this.nudPrice.Location = new System.Drawing.Point(326, 55);
            this.nudPrice.Name = "nudPrice";
            this.nudPrice.Size = new System.Drawing.Size(120, 20);
            this.nudPrice.TabIndex = 12;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(863, 137);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(178, 23);
            this.btnClear.TabIndex = 6;
            this.btnClear.Text = "Clear Fields";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(486, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Type";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(78, 54);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(158, 20);
            this.txtName.TabIndex = 6;
            // 
            // txtItemID
            // 
            this.txtItemID.Enabled = false;
            this.txtItemID.Location = new System.Drawing.Point(78, 28);
            this.txtItemID.Name = "txtItemID";
            this.txtItemID.Size = new System.Drawing.Size(100, 20);
            this.txtItemID.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(254, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Weight";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(254, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Armor Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ItemID";
            // 
            // Armor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 523);
            this.Controls.Add(this.AddView);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.dgvArmor);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Armor";
            this.Text = "Armor";
            this.Load += new System.EventHandler(this.Armor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvArmor)).EndInit();
            this.AddView.ResumeLayout(false);
            this.AddView.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudArmor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvArmor;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.GroupBox AddView;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtItemID;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.NumericUpDown nudPrice;
        private System.Windows.Forms.NumericUpDown nudArmor;
        private System.Windows.Forms.NumericUpDown nudWeight;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.ComboBox cbMaterial;
        private System.Windows.Forms.ComboBox cbAS;
        private System.Windows.Forms.ComboBox cbPart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
    }
}