﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWeapon = new System.Windows.Forms.Button();
            this.btnArmor = new System.Windows.Forms.Button();
            this.btnSpellls = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnWeapon
            // 
            this.btnWeapon.Location = new System.Drawing.Point(25, 170);
            this.btnWeapon.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWeapon.Name = "btnWeapon";
            this.btnWeapon.Size = new System.Drawing.Size(147, 70);
            this.btnWeapon.TabIndex = 0;
            this.btnWeapon.Text = "Weapon";
            this.btnWeapon.UseVisualStyleBackColor = true;
            this.btnWeapon.Click += new System.EventHandler(this.btnWeapon_Click);
            // 
            // btnArmor
            // 
            this.btnArmor.Location = new System.Drawing.Point(221, 170);
            this.btnArmor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnArmor.Name = "btnArmor";
            this.btnArmor.Size = new System.Drawing.Size(147, 70);
            this.btnArmor.TabIndex = 1;
            this.btnArmor.Text = "Armor";
            this.btnArmor.UseVisualStyleBackColor = true;
            this.btnArmor.Click += new System.EventHandler(this.btnArmor_Click);
            // 
            // btnSpellls
            // 
            this.btnSpellls.Location = new System.Drawing.Point(421, 170);
            this.btnSpellls.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSpellls.Name = "btnSpellls";
            this.btnSpellls.Size = new System.Drawing.Size(147, 70);
            this.btnSpellls.TabIndex = 2;
            this.btnSpellls.Text = "Spells";
            this.btnSpellls.UseVisualStyleBackColor = true;
            this.btnSpellls.Click += new System.EventHandler(this.BtnSpellls_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.btnSpellls);
            this.Controls.Add(this.btnArmor);
            this.Controls.Add(this.btnWeapon);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWeapon;
        private System.Windows.Forms.Button btnArmor;
        private System.Windows.Forms.Button btnSpellls;
    }
}

