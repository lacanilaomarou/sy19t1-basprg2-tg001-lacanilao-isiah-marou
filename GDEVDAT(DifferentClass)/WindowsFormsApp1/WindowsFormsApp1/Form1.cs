﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnWeapon_Click(object sender, EventArgs e)
        {
           
            Weapon WeaponForm = new Weapon();
            WeaponForm.Text = "Weapon";
            WeaponForm.Show();
        }

        private void btnArmor_Click(object sender, EventArgs e)
        {
            Armor ArmorForm = new Armor();
            ArmorForm.Text = "Armor";
            ArmorForm.Show();
        }

        private void BtnSpellls_Click(object sender, EventArgs e)
        {
            Spells SpellForm = new Spells();
            SpellForm.Text = "Spell";
            SpellForm.Show();
        }
    }
}
