#include "Crystal.h"

Crystal::Crystal(string name, int value,Unit* unit):
	Item(name, unit)
{
	mCrystals = value;
}

Crystal::~Crystal()
{
}

void Crystal::Message()
{
	cout << "You Picked up a " << this->GetName() << endl;
	cout << "Your received additional " << mCrystals << " Crystals\n\n";
}

void Crystal::Effect()
{
	this->GetUnit()->AddCrystals(mCrystals);
}
