#pragma once
#include "Item.h"
class Rare :
	public Item
{
private:
	int mPoints;
public:
	Rare(string name, int points, Unit* unit);
	~Rare();

	int GetPointsValue();

	void Message() override;
	void Effect() override;
};

