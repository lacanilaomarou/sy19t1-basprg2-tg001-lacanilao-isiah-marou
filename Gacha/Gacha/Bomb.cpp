#include "Bomb.h"

Bomb::Bomb(string name, int value, Unit* unit):
	Item(name, unit)
{
	mDamage = value;
}

Bomb::~Bomb()
{
}

int Bomb::GetDamage()
{
	return mDamage;
}

void Bomb::Message()
{
	cout << "You Picked up a " << this->GetName() << endl;
	cout << "Your received " << mDamage << " HP damage\n\n";
}

void Bomb::Effect()
{
	this->GetUnit()->DamageHP(mDamage);
}
