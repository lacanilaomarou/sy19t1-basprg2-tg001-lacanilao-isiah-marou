#pragma once
#include "Item.h"
class Bomb :
	public Item
{
private:
	int mDamage;
public:
	Bomb(string name, int value, Unit* unit);
	~Bomb();

	int GetDamage();
	void Message() override;
	void Effect() override;
};

