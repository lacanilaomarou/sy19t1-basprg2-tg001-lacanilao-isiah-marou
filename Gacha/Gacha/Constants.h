#pragma once
#include <string>
using namespace std;

const int PullCost = 5;
const int GoalPoints = 100;
const int SSR_Points = 50;
const int SR_Points = 10;
const int R_Points = 1;
const int HealthPotion_HealValue = 30;
const int Bomb_Damage = 25;
const int Additional_Crystals = 15;
const string Name_SSR = "SSR Item";
const string Name_SR = "SR Item";
const string Name_R = "R Item";
const string Name_HealthPotion = "Health Potion";
const string Name_Bomb = "Bomb";
const string Name_Crystals = "Crystals";
