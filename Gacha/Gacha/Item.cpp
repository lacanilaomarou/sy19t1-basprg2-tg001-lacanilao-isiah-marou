#include "Item.h"


Item::Item(string name, Unit* unit)
{
	mName = name;
	mUnit = unit;
}

Item::~Item()
{
}

string Item::GetName()
{
	return mName;
}

void Item::Message()
{
	cout << "YOU RECEIVED NOTHIN!" << endl;
}

void Item::Effect()
{
	cout << "NO ITEM, NO EFFECT!" << endl;
}

Unit* Item::GetUnit()
{
	return mUnit;
}
