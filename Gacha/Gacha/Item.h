#pragma once
#include <iostream>
#include<string>
#include "Unit.h"
using namespace std;

class Item
{
private:
	string mName;
	Unit* mUnit;
public:
	Item(string name, Unit* unit);
	~Item();

	string GetName();

	virtual void Message();
	virtual void Effect();

	Unit* GetUnit();
};

