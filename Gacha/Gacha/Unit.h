#pragma once
#include <iostream>
#include<string>
#include <vector>
using namespace std;

class Item;
class Unit
{
private:
	string mName;

	int mMaxHP;
	int mCurrentHP;
	int mCrystalsLeft;
	int mPointsAcquired;

	vector <Item*> items;
public:
	Unit(string name);
	~Unit();

	string GetName();
	int GetCurrentHp();
	int GetCrystalsAmount();
	int GetPointsAcquired();

	void DamageHP(int value);
	void HealHP(int value);
	void ReduceCrystals(int value);
	void AddPointsAcquired(int value);
	void AddCrystals(int value);

	void AddItems(Item* ToAdd);
	Item* GetItems(int index);
	vector<Item*> GetVectorItems();

	int GetItemsSize();
	void ShowStats();
};

