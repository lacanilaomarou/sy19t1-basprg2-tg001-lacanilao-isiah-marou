#include <iostream>
#include <string>
#include <time.h>
#include <algorithm>
#include "Unit.h"
#include "Item.h"
#include "Rare.h"
#include "Potion.h"
#include "Bomb.h"
#include "Crystal.h"
#include "Constants.h"

using namespace std;

void PullRandomItem(Unit* unit, int index); //Add Random Item//
void ListItemsPulled(Unit* unit); //list all items//
void CollateItemsPulled(Unit* unit); //number of each item//
bool CheckIfContinue(Unit* unit); // check if player is alive and have enough crystals //
void Evaluation(Unit* unit); //Final eval if goal is met//
void Match(); // play the whole match//
char AskToPlayAgain(); // Ask for another match

int main()
{
	srand(time(0));

	char again = 'y';

	while (again ==  'y')
	{
		Match();

		again = AskToPlayAgain();
	}
	cout << "Thank You For Playing!\n";
	return 0;
}

void PullRandomItem(Unit* unit, int index)
{
	float rng = (float)rand() / RAND_MAX;

	// FOR CHECKING ONLY //
	//cout << "\nRNG: " << rng << endl;


	unit->ReduceCrystals(PullCost);
	// Pull SSR, 1% chance//
	if ((rng > 0) && (rng <= 0.01f))
	{
		Rare* SSR = new Rare(Name_SSR, SSR_Points, unit);
		unit->AddItems(SSR);
	}

	// Pull SR, 9% chance//
	else if ((rng > 0.01f) && (rng <= 0.1f))
	{
		Rare* SR = new Rare(Name_SR, SR_Points, unit);
		unit->AddItems(SR);
	}

	//Pull R, 40% chance//
	else if ((rng > 0.1f) && (rng <= 0.5f))
	{
		Rare* R = new Rare(Name_R, R_Points, unit);
		unit->AddItems(R);
	}

	//Pull Potion, 15% chance//
	else if ((rng > 0.5f) && (rng <= 0.65f))
	{
		Potion* potion = new Potion(Name_HealthPotion, HealthPotion_HealValue, unit);
		unit->AddItems(potion);
	}

	//Pull Bomb, 20% chance//
	else if ((rng > 0.65f) && (rng <= 0.85f))
	{
		Bomb* bomb = new Bomb(Name_Bomb, Bomb_Damage, unit);
		unit->AddItems(bomb);
	}

	//Pull Crystals, 15% chance//
	else if ((rng > 0.85f) && (rng <= 1.0f))
	{
		Crystal* crystal = new Crystal(Name_Crystals, Additional_Crystals, unit);
		unit->AddItems(crystal);
	}
	unit->GetItems(index)->Message();
	unit->GetItems(index)->Effect();
}

void ListItemsPulled(Unit* unit)
{
	cout << "List of items Pulled: " << endl;
	for (int i = 0; i < unit->GetItemsSize(); i++)
	{
		cout << i+1 << ". " << unit->GetItems(i)->GetName() << endl;
	}
	cout << endl << endl;
	system("pause");
	system("cls");
}

void CollateItemsPulled(Unit* unit)
{
	// SYNTAX FOR NOOBS //
	/*int SSRcount = 0, SRCount = 0, RCount = 0, PotionCount = 0, BombCount = 0, CrystalCount = 0;
	for (int i = 0; i < unit->GetItemsSize(); i++)
	{
		if (unit->GetItems(i)->GetName() == Name_SSR) SSRcount++;
		if (unit->GetItems(i)->GetName() == Name_SR) SRCount++;
		if (unit->GetItems(i)->GetName() == Name_R) RCount++;
		if (unit->GetItems(i)->GetName() == Name_HealthPotion) PotionCount++;
		if (unit->GetItems(i)->GetName() == Name_Bomb) BombCount++;
		if (unit->GetItems(i)->GetName() == Name_Crystals) CrystalCount++;
	}*/


	//SYNTAX FOR PROS//
	vector <string> counter;
	for (int i = 0; i < unit->GetItemsSize(); i++)
	{
		counter.push_back(unit->GetItems(i)->GetName());
	}
	int SSRcount = count(counter.begin(), counter.end(), Name_SSR);
	int SRCount = count(counter.begin(), counter.end(), Name_SR);
	int RCount = count(counter.begin(), counter.end(), Name_R);
	int PotionCount = count(counter.begin(), counter.end(), Name_HealthPotion);
	int BombCount = count(counter.begin(), counter.end(), Name_Bomb);
	int CrystalCount = count(counter.begin(), counter.end(), Name_Crystals);
	

	cout << "Breakdown of Items Pulled: " << endl;
	cout << "=======================================\n";
	cout << "SSR x" << SSRcount << " (" << SSRcount * SSR_Points << " points acquired)"<<endl;
	cout << "SR x" << SRCount << " (" << SRCount* SR_Points << " points acquired)"<< endl;
	cout << "R x" << RCount << " (" << RCount << " points acquired)" << endl;
	cout << "Potions x" << PotionCount << " (" << PotionCount * HealthPotion_HealValue << " HP healed overall)"<<endl;
	cout << "Bombs x " << BombCount << " (" << BombCount * Bomb_Damage << " HP damaged overall)"<<endl;
	cout << "Crystals x" << CrystalCount << " (" << CrystalCount * Additional_Crystals << " crystals gained during the match)" << endl;
	cout << "=======================================" << "\n\n";
	cout << "TOTAL OF ITEMS PULLED: " << unit->GetItemsSize() << endl;
	system("pause");
	system("cls");
	counter.erase(counter.begin(), counter.end());
}

bool CheckIfContinue(Unit* unit)
{
	if (unit->GetCurrentHp() <= 0)cout << "You DIED\n";
	system("cls");
	return ((unit->GetCurrentHp() >0)&&(unit->GetCrystalsAmount() >0));
}

void Evaluation(Unit* unit)
{

	cout << "FINAL STATS:" << endl;
	unit->ShowStats();
	cout << "TOTAL OF ITEMS PULLED: " << unit->GetItemsSize() << endl;
	cout << "=======================================" << endl;

	if (unit->GetCurrentHp() <= 0)
	{
		cout << "YOU DIED\nYOU LOST MISERABLY";
	}
	else if (unit->GetPointsAcquired() >= GoalPoints)
	{
		cout << "YOU REACHED THE GOAL, CONGRATULATIONS!\nYOU WON!";
	}
	else 
	{
		cout << "YOU SPENT ALL OF YOUR CRYSTALS WITHOUT REACHING THE GOAL\nYOU LOST";
	}
	cout << "\n\n\n\n";
	system("pause");
	system("cls");

}

void Match()
{
	Unit* unit = new Unit("Player");

	int i = 0;

	while (CheckIfContinue(unit))
	{
		unit->ShowStats();

		cout << "Pulls: " << i << "\n\n\n";

		PullRandomItem(unit, i);
		i++;
		system("pause");
		system("cls");
	}

	ListItemsPulled(unit);
	CollateItemsPulled(unit);
	Evaluation(unit);

	delete unit;
}

char AskToPlayAgain()
{
	char again;
	cout << "Do you want to Play again? (y)YES (Any)No" << endl;
	cin >> again;
	cout << endl;
	system("pause");
	system("cls");
	return again;
}
