#include "Potion.h"

Potion::Potion(string name, int value, Unit* unit) :
	Item(name, unit)
{
	mHealValue = value;
}

Potion::~Potion()
{
}

int Potion::GetHealValue()
{
	return mHealValue;
}

void Potion::Message()
{
	cout << "You Picked up a " << this->GetName() << endl;
	cout << "You gained " << mHealValue << " HP\n\n";
}

void Potion::Effect()
{
	this->GetUnit()->HealHP(mHealValue);
	
}
