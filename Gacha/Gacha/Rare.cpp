#include "Rare.h"

Rare::Rare(string name, int points, Unit* unit):
	Item(name, unit)
{
	mPoints = points;
}

Rare::~Rare()
{
}

int Rare::GetPointsValue()
{
	return mPoints;
}

void Rare::Message()
{
	cout << "You Picked up a " << this->GetName() << endl;
	cout << "You gained " << mPoints << " Points\n\n";
}

void Rare::Effect()
{
	this->GetUnit()->AddPointsAcquired(mPoints);
}


