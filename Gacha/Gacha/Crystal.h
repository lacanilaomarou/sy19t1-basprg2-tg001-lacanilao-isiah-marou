#pragma once
#include "Item.h"
class Crystal: public Item
{
private:
	int mCrystals;
public:
	Crystal(string name, int value, Unit* unit);
	~Crystal();

	void Message() override;
	void Effect() override;
};

