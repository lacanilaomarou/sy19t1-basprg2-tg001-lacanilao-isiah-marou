#include "Unit.h"

Unit::Unit(string name)
{
	mName = name;
	mCurrentHP = 100;
	mCrystalsLeft = 100;
	mPointsAcquired = 0;
}

Unit::~Unit()
{
	for (int i = 0; i < items.size(); i++)
	{
		delete items[i];
	}
}

string Unit::GetName()
{
	return mName;
}



int Unit::GetCurrentHp()
{
	return mCurrentHP;
}

int Unit::GetCrystalsAmount()
{
	return mCrystalsLeft;
}

int Unit::GetPointsAcquired()
{
	return mPointsAcquired;
}

void Unit::DamageHP(int value)
{
	mCurrentHP -= value;
	if (mCurrentHP < 0) mCurrentHP = 0;
}

void Unit::HealHP(int value)
{
	mCurrentHP += value;
}

void Unit::ReduceCrystals(int value)
{
	mCrystalsLeft -= value;
}

void Unit::AddPointsAcquired(int value)
{
	mPointsAcquired += value;
}
void Unit::AddCrystals(int value)
{
	mCrystalsLeft += value;
}
void Unit::AddItems(Item* ToAdd)
{
	items.push_back(ToAdd);
}

Item* Unit::GetItems(int index)
{
	return items[index];
}

vector<Item*> Unit::GetVectorItems()
{
	return items;
}

int Unit::GetItemsSize()
{
	return items.size();
}

void Unit::ShowStats()
{
	cout << "=======================================" << endl;
	cout << "HP Left: " << mCurrentHP  << endl;
	cout << "Crystals Left: " << mCrystalsLeft << endl;
	cout << "Rarity points Acquired: " << mPointsAcquired << endl;
	cout << "=======================================" << endl;
}
