#pragma once
#include "Item.h"
class Potion: public Item
{
private:
	int mHealValue;
public:
	Potion(string name, int value, Unit* unit);
	~Potion();

	int GetHealValue();

	void Message() override;
	void Effect();
};

