#include "Unit.h"
#include <iostream>

using namespace std;

Unit::Unit(string name)
{
	mName = name;
	mClass = "Classless";
	mMaxHp = 0;
	mCurrentHP = 0;
	mPow = 0;
	mVit = 0;
	mAgi = 0;
	mDex = 0;
	mStrongTo = "N/A";
}

Unit::~Unit()
{

}

string Unit::GetName()
{
	return mName;
}

string Unit::GetClass()
{
	return mClass;
}

int Unit::GetMaxHP()
{
	return mMaxHp;
}

void Unit::AddMaxHp(int value)
{
	mMaxHp += value;
}

void Unit::setMaxHP(int value)
{
	mMaxHp = value;
}

int Unit::GetCurrentHP()
{
	return mCurrentHP;
}

void Unit::setCurentHP(int value)
{
	mCurrentHP = value;
}

void Unit::DamageCurrentHP(int dmg)
{
	mCurrentHP = mCurrentHP - dmg;
	if (mCurrentHP <= 0) mCurrentHP = 0;
}

void Unit::AfterRoundHealCurrentHP()
{
	int temp = mMaxHp * 0.3;
	mCurrentHP += temp;
	if (mCurrentHP > mMaxHp) mCurrentHP = mMaxHp;
}

int Unit::GetPow()
{
	return mPow;
}

void Unit::AddPow(int value)
{
	mPow += value;
}

int Unit::GetVit()
{
	return mVit;
}

void Unit::AddVit(int value)
{
	mVit += value;
}

int Unit::GetAgi()
{
	return mAgi;
}

void Unit::AddAgi(int value)
{
	mAgi += value;
}

int Unit::GetDex()
{
	return mDex;
}

void Unit::AddDex(int value)
{
	mDex += value;
}

void Unit::SetStrong(string Cla)
{
	mStrongTo = Cla;
}

int Unit::GetDamageFor(Unit* Target)
{
	int bonusmult = 100;
	if (this->mStrongTo == Target->GetClass()) bonusmult = 150;
	int VITDef = Target->GetVit();
	int dmg = ((this->mPow - VITDef) / 100) * bonusmult;

	if (dmg <= 0) dmg = 1;



	return dmg;
}

bool Unit::DiditHit(Unit* Target)
{
	int AGIDef = Target->GetAgi();
	int HitRate = (this->mDex / AGIDef) * 100;

	if (HitRate <= 20) HitRate = 20;
	if (HitRate >= 80) HitRate = 80;

	int rng = rand() % 100 + 1;

	//cout << endl << "HitRate: " << HitRate << endl << "RNG: " << rng << endl << endl;

	if (HitRate >= rng) return true;

	return false;
}

void Unit::Attack(Unit* Target)
{


	if (this->DiditHit(Target) == true)
	{
		int dmg = GetDamageFor(Target);
		Target->DamageCurrentHP(dmg);
		cout << endl << this->mName << " attacked " << Target->GetName() << " for " << dmg << ", " << Target->GetName() << "'s current HP is " << Target->GetCurrentHP() << endl;
	}
	else cout << endl << this->mName << " missed" << endl;
}

void Unit::ShowStats()
{
	cout << endl << mName << "'s STATS" << endl;
	cout << "Class: " << mClass << endl << "HP: " << mCurrentHP << endl;
	cout << "POWER: " << mPow << endl << "Vitality: " << mVit << endl << "Agility: " << mAgi << endl << "Dexterity: " << mDex << endl << endl;
}

void Unit::SetUpClass(int choice, int handicap)
{
	switch (choice)
	{
	case 1:
		mClass = string_warrior;
		mStrongTo = string_assassin;
		mMaxHp = 13 + handicap;
		mCurrentHP = mMaxHp;
		mVit = 13 + handicap;
		mAgi = 10 + handicap;
		mDex = 10 + handicap;
		mPow = 10 + handicap;
		break;
	case 2:
		mClass = string_assassin;
		mStrongTo = string_mage;
		mMaxHp = 10 + handicap;
		mCurrentHP = mMaxHp;
		mVit = 10 + handicap;
		mAgi = 13 + handicap;
		mDex = 13 + handicap;
		mPow = 10 + handicap;
		break;
	case 3:
		mClass = string_mage;
		mStrongTo = string_warrior;
		mMaxHp = 10 + handicap;
		mCurrentHP = mMaxHp;
		mVit = 10 + handicap;
		mAgi = 10 + handicap;
		mDex = 10 + handicap;
		mPow = 15 + handicap;
		break;
	default:
		break;
	}
}
