#pragma once
#include <string>
using namespace std;

const string string_warrior = "Warrior";
const string string_assassin = "Assassin";
const string string_mage = "Mage";

class Unit
{
private:
	string mName;
	string mClass;
	int mMaxHp;
	int mCurrentHP;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	string mStrongTo;

public:
	Unit(string name);
	~Unit();


	string GetName();

	string GetClass();

	int GetMaxHP();
	void AddMaxHp(int value);
	void setMaxHP(int value);

	int GetCurrentHP();
	void setCurentHP(int value);

	void DamageCurrentHP(int dmg);
	void AfterRoundHealCurrentHP();

	int GetPow();
	void AddPow(int value);

	int GetVit();
	void AddVit(int value);

	int GetAgi();
	void AddAgi(int value);

	int GetDex();
	void AddDex(int value);

	void SetStrong(string Cla);

	int GetDamageFor(Unit* Target);
	bool DiditHit(Unit* Target);

	void Attack(Unit* Target);

	void ShowStats();

	void SetUpClass(int choice, int handicap);
};



