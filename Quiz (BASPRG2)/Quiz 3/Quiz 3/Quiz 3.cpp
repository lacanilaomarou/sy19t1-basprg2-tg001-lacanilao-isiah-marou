#include <iostream>
#include "Unit.h"
#include <time.h>

using namespace std;

void SetUp(Unit* Player);
void Combat(Unit* first, Unit* second);
void PlayerReward(Unit* Player, Unit* Opponent);
void Match(Unit* Player);


int main()
{
	srand(time(NULL));
	string name;
	cout << "Whats your name? ";
	cin >> name;
	Unit* Player = new Unit(name);

	SetUp(Player);
	Player->ShowStats();
	system("pause");
	system("cls");

	Match(Player);
	delete Player;
}

void SetUp(Unit* Player)
{
	int choice;

	cout << "What class do you want? (1)Warrior (2)Assassin (3)Mage" << endl;
	cin >> choice;

	Player->SetUpClass(choice, 0);
}

void Combat(Unit* first, Unit* second)
{
	while (true)
	{
		first->Attack(second);
		if (second->GetCurrentHP() <= 0) break;
		system("pause");

		second->Attack(first);
		if (first->GetCurrentHP() <= 0) break;
		system("pause");
	}
}

void PlayerReward(Unit* Player, Unit* Opponent)
{
	if (Opponent->GetClass() == string_warrior)
	{
		Player->AddMaxHp(3);
		Player->AddVit(3);
	}
	else if (Opponent->GetClass() == string_assassin)
	{
		Player->AddAgi(3);
		Player->AddDex(3);
	}
	else if (Opponent->GetClass() == string_mage)
	{
		Player->AddPow(3);
	}
	Player->AfterRoundHealCurrentHP();
	system("pause");
	cout << endl << Player->GetName() << " gained some points" << endl;
	Player->ShowStats();
	system("pause");
	system("cls");
}

void Match(Unit* Player)
{
	bool playerwin = true;

	int handicap = -3;

	while (playerwin == true)
	{
		int rng = rand() % 3 + 1;
		int HandicapScale = rand() % 3;


		Unit* Opponent = new Unit("Opponent");
		Opponent->SetUpClass(rng, handicap);

		cout << "a(n) " << Opponent->GetClass() << " has spawned" << endl;
		Opponent->ShowStats();

		system("pause");

		if (Player->GetAgi() >= Opponent->GetAgi()) Combat(Player, Opponent);
		else Combat(Opponent, Player);

		if (Player->GetCurrentHP() == 0)
		{
			cout << endl << Player->GetName() << " died" << endl;
			playerwin = false;
		}
		else
		{
			cout << endl << Opponent->GetName() << " died" << endl;
			PlayerReward(Player, Opponent);
			delete Opponent;
			handicap += HandicapScale;
		}

	}
	system("pause");
	system("cls");
	cout << "You defeated " << handicap + 3 << " Opponents, Good Job";
}