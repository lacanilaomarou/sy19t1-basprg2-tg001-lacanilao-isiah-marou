#include <iostream>
#include <time.h>
#include <string>

using namespace std;

int sort(int arr[], int s);
void print(int array[]);
int buy(int money, int price, int packages[]);
int getprice();

int main()
{
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int money = 250;
	int price = 0;
	int suggest;
	string asktocont;

	cout << "Money: " << money << endl << endl;

	sort(packages, 7);

	while (true)
	{
		print(packages); // print packages
		price = getprice(); // get price
		suggest = buy(money, price, packages); // suggest and get change

		system("pause");
		system("cls");

		cout << "Do you want to buy more? (Enter any key for YES)(Enter 'n' for NO)\n";
		cin >> asktocont;

		if (asktocont == "n")
		{
			cout << "Thank You For Playing!\n";
			system("pause");
			break;
		}
		system("cls");
	}

	return 0;
}

int getprice()
{
	int price;
	do
	{
		cout << "How much is the item you want to buy? ";
		cin >> price;
		if (price > 0)
		{
			break;
		}
	} while (price < 0);
	return price;
}

void print(int array[])
{
	for (int i = 0; i < 7; i++)
	{
		cout << "Package #" << i + 1 << ": " << array[i] << endl;
	}
	cout << endl;
}

int sort(int arr[], int s)
{
	int temp;
	for (int i = 0; i < s; i++)
	{
		for (int x = i + 1; x < s; x++)
		{
			if (arr[x] < arr[i])
			{
				temp = arr[i];
				arr[i] = arr[x];
				arr[x] = temp;
			}
		}
	}
	return *arr;
}

int buy(int money, int price, int packages[])
{
	int dif = price - money;
	int suggest = 0, i;
	bool suf = false;

	for (i = 0; i < 7; ++i)
	{
		if (dif < packages[i])
		{
			suggest = packages[i];
			suf = true;
			break;
		}
	}
	if (dif <= 0)
	{
		money -= price;
	}
	else
	{
		cout << "\nNot enough gold to but this item\n";

		if (suf == true)
		{
			while (true)
			{
				string buyy;
				cout << "\nDo you want to purchase Package #" << i + 1 << "?\n(Enter 'y' for YES)(Enter 'n' for NO)\n";
				cin >> buyy;

				if (buyy == "y")
				{
					cout << "\nYou Purchased Package #" << i + 1 << ": " << suggest << " Package" << endl;
					money += suggest;
					money -= price;
					break;
				}
				else if (buyy == "n")
				{
					cout << "\nItem not bought\n";
					break;
				}
			}
		}
		else if (suf == false)
		{
			cout << "\nNo IAP package is suitable for this purchase\n\n";
		}

	}

	cout << endl << "Money update: " << money << endl << endl;
	return money;
}

