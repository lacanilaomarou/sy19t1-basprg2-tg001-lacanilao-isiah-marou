#pragma once
#include "Skills.h"
#include <string>
using namespace std;
class Skills;

class Heal: public Skills
{
private:
	string mName = "Heal";
public:
	Heal();
	~Heal();
	void UseSkill(Unit* unit, int value) override;
};

