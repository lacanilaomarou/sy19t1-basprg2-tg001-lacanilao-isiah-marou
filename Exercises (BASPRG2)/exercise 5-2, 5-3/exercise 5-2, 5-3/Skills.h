#pragma once
#include <string>
#include "Unit.h"
using namespace std;

class Skills
{
private:
	Unit munit;
public:
	Skills(Unit* unit);
	~Skills();

	virtual void UseSkill(Unit* unit, int value);
};

