#pragma once
#include <string>
#include <iostream>
#include "Skills.h"
#include "Stats.h"
using namespace std;

class Skills;

class Unit
{
private:
	string mName;
	Stats stats;
	Skills* mSkill;
public:
	Unit(string name);
	~Unit();

	void addMaxHP(int value);
	void ShowStats();
	void setSpell(Skills* spell);
};

