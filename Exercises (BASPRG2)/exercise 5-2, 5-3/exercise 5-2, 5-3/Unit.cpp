#include "Unit.h"

Unit::Unit(string name)
{
	Stats* stats = new Stats();
	mName = name;
}

Unit::~Unit()
{
}

void Unit::addMaxHP(int value)
{
	stats.mMaxHP += value;
}

void Unit::ShowStats()
{
	cout << "HP: " << stats.mMaxHP << endl;
	cout << "Might: " << stats.mMight << endl;
	cout << "Vitality: " << stats.mVitality << endl;
	cout << "Dexterity: " << stats.mDexterity << endl;
	cout << "Agility: " << stats.mAgility << endl;
	
}

void Unit::setSpell(Skills* skill)
{
	if (mSkill != nullptr) delete mSkill;
	mSkill = skill;
}
