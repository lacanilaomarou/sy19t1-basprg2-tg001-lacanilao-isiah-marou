#include <iostream>
#include <time.h>
#include <string>

using namespace std;

// ex 2-1, plac bet only
/*
int placebet(int gold)
{
	int bet;
	bool brek = false;
	do
	{
		cout << "Place Bet: ";
		cin >> bet;
		if ((bet > 0) && (bet <= gold))
		{
			brek = true;
		}
	} while (brek == false);

	return bet;
}*/


//2-1, int place bet and deduct money
/*
int placebet(int& gold)
{
	int bet;
	bool brek = false;
	do
	{
		cout << "Place Bet: ";
		cin >> bet;
		if ((bet > 0) && (bet <= gold))
		{
			brek = true;
		}
	} while (brek == false);
	gold -= bet;
	
	return bet;
}
*/


//2-1, void, reference place bet on money
void placebet(int& gold, int& bet)
{
	bool brek = false;
	do
	{
		cout << "Place Bet: ";
		cin >> bet;
		if ((bet > 0) && (bet <= gold))
		{
			brek = true;
		}
	} while (brek == false);
	gold -= bet;
}


//2-2 dice roll
void diceroll (int& rollerroll, string turn)
{
	int dice[2];
	for (int i =0; i < 2; i++)
	{
		dice[i] = rand() % 6 + 1;
	}

	//couts
	cout <<endl << turn << "'s turn" << endl;

	for (int i = 0; i < 2; i++)
	{
		cout << "dice #" << i + 1 << " : " << dice[i] << endl;
	}
	cout << endl << endl;

	rollerroll = dice[0] + dice[1];
}

// 2-3 Evaluation
void payout(int bet, int& gold, int playerroll, int airoll)
{
	cout << "Player roll: " << playerroll << endl << "AI roll: " << airoll << endl;
	if (playerroll == airoll)
	{
		gold += bet;
		cout << "Its a draw";
	}
	else if ((playerroll == 2) || (airoll == 2))
	{
		if (playerroll == 2)
		{
			cout << "You won double your bet";
			gold += bet * 3;
		}
		if (airoll == 2)
		{
			cout << "YOu lost";
		}
	}
	else if (playerroll > airoll)
	{
		cout << "You won";
		gold += bet * 2;
	}
	else if (playerroll < airoll)
	{
		cout << "you lost your bet";
	}
	cout << endl;
}

// 2-4 check if lose or not
void loopendcheck(int gold, bool& loopend)
{
	if (gold <= 0)
	{
		system("pause");
		system("cls");
		cout << "You lost all of your money" << endl;
		loopend = true;
	}
	
	else
	{
		system("pause");
		system("cls");
	}
}




int main()
{
	srand(time(0));
	int gold = 1000, bet;
	int playerroll, airoll;
	bool loopend = false;

	do 
	{
		cout << "Gold: " << gold << endl;

		//bet = placebet(gold); gold -= bet;							// placebet 2-1 // "NO REFERENCE"
		//bet = placebet(gold);										// placebet 2-1 // "REFERENCE GOLD RETURN BET"
		placebet(gold, bet);											// placebet 2-1 // "VOID"


		diceroll(playerroll, "Player"); // player roll 2-2


		diceroll(airoll, "AI"); // ai roll 2-2


		payout(bet, gold, playerroll, airoll); // 2-3

		
		loopendcheck(gold, loopend); // 2-4
		
	} while (loopend == false);

	return 0;


}

