#include <iostream>
using namespace std;

/// Exercise 1-1

int fact(int n)
{
	int x = 1;
	for (int i = 2; i <= n; i++)
	{
		x = x * i;
	}
	return x;
}

int main()
{
	int n;
	cout << "enter a number: ";
	cin >> n;
	cout << "Factorial of " << n << " = " << fact(n) << endl;
	return 0;
}