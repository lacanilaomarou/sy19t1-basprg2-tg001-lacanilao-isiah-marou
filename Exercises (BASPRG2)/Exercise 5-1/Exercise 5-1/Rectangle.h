#pragma once
#include "Shape.h"
class Rectangle: public Shape
{
private:
	int mLength;
	int mWidth;
	int mArea = mLength * mWidth;
public:
	Rectangle();
	~Rectangle();

	int GetLength();
	int GetWidth();

	void SetLength(int value);
	void SetWidth(int value);

	int GetArea() override;
};

