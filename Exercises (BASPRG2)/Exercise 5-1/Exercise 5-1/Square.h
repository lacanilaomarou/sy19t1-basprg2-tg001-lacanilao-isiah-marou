#pragma once
#include "Shape.h"
class Square: public Shape
{
private:
	int mLength;
	int mArea = mLength * mLength;
public:
	Square();
	~Square();


	int GetLength();
	void SetLength(int value);

	int GetArea() override;
};

