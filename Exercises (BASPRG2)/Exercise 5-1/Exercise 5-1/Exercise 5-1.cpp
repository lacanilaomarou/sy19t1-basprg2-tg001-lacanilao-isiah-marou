// Exercise 5-1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>
#include "Shape.h"
#include "Square.h"
#include "Rectangle.h"
#include "Circle.h"
using namespace std;

int main()
{
	vector<Shape*> shapes;

	Square* square = new Square();
	square->SetName("Square");
	square->SetNumSides(4);
	square->SetLength(6);
	shapes.push_back(square);

	Rectangle* rec = new Rectangle();
	rec->SetName("Rectangle");
	rec->SetNumSides(4);
	rec->SetLength(9);
	rec->SetWidth(4);
	shapes.push_back(rec);

	Circle* circle = new Circle();
	circle->SetName("Circle");
	circle->SetNumSides(0);
	circle->SetRadius(8);
	shapes.push_back(circle);

	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		cout << shape->GetName() << "\nSides: " << shape->GetNumSides() << "\nArea: " << shape->GetArea() << "\n\n";
	}
}

