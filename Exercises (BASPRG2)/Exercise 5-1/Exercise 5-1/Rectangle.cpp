#include "Rectangle.h"

Rectangle::Rectangle()
{
}

Rectangle::~Rectangle()
{
}

int Rectangle::GetLength()
{
	return mLength;
}

int Rectangle::GetWidth()
{
	return mWidth;
}

void Rectangle::SetLength(int value)
{
	mLength = value;
}

void Rectangle::SetWidth(int value)
{
	mWidth = value;
}

int Rectangle::GetArea()
{
	return mLength * mWidth;
}
