#pragma once
#include <string>
using namespace std;
class Shape
{
private:
	string mName;
	int mNumSlides;
public:
	Shape();
	~Shape();

	string GetName();
	int GetNumSides();

	void SetName(string name);
	void SetNumSides(int value);

	virtual int GetArea();
};

