#include "Circle.h"

Circle::Circle()
{
}

Circle::~Circle()
{
}

int Circle::GetRadius()
{
	return mRadius;
}

void Circle::SetRadius(int value)
{
	mRadius = value;
}

int Circle::GetArea()
{
	return mArea = (mRadius * mRadius) * 3.14;
}
