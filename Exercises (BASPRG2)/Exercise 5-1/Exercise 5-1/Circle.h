#pragma once
#include "Shape.h"
class Circle: public Shape
{
private:
	int mRadius;
	int mArea = (mRadius * mRadius) * 3.14;
public:
	Circle();
	~Circle();

	int GetRadius();
	void SetRadius(int value);

	int GetArea() override;
};

