#include <iostream>
#include <time.h>

using namespace std;

int fill(int arr[])
{
	srand(time(0));
	for (int i = 0; i < 10; i++)
	{
		arr[i] = rand() % 100 + 1;
	}
	return *arr;
}

void print(int arr[])
{
	for (int i = 0; i < 10; i++)
	{
		cout << arr[i] << " , ";
	}
	cout << endl << endl;
}

int largest(int arr[])
{
	int x = 0;
	for (int i = 0; i < 10; i++)
	{
		if (x < arr[i])
		{
			x = arr[i];
		}
	}
	return x;
}

int sort(int arr[])
{
	int temp;
	for (int i = 0; i < 10; i++)
	{
		for (int x = i + 1; x < 10; x++)
		{
			if (arr[x] < arr[i])
			{
				temp = arr[i];
				arr[i] = arr[x];
				arr[x] = temp;
			}
		}
	}
	return *arr;
}

int main()
{
	int arr[10];
	int sortasc[10];
	fill(arr); /// 1-4
	print(arr);		//1-4

	cout << "largest number in the array: " << largest(arr) << endl << endl; // 1-5

	copy(arr, arr + 10, sortasc);
	sort(sortasc);
	print(sortasc);


	return(0);
}