#include <iostream>
#include <time.h>
#include "Skill.h"
#include "Wizard.h"

using namespace std;

int main()
{
	srand(time(0));
	string name[2];
	for (int i = 0; i < 2; i++)
	{
		cout << "What is this wizards name? ";
		cin >> name[i];
		cout << endl;
	}
	Wizard* fwizard = new Wizard(name[0]);
	Wizard* swizard = new Wizard(name[1]);

	while (1)
	{
		fwizard->UseSkill(swizard);
		cout << fwizard->GetName() << " used " << fwizard->GetSkillName() << endl;
		cout << swizard->GetName() << " 's HP Status: " << swizard->getCurrentHp() << endl;
		if (swizard->getCurrentHp() <= 0)
		{
			system("pause");
			system("cls");
			cout << fwizard->GetName() << " Won!";
			break;
		}


		swizard->UseSkill(fwizard);
		cout << swizard->GetName() << " used " << swizard->GetSkillName() << endl;


		cout << fwizard->GetName() << " 's HP Status: " << fwizard->getCurrentHp() << endl  << endl;

		if (fwizard->getCurrentHp() <=0) 
		{
			system("pause");
			system("cls");
			cout << swizard->GetName() << " Won!";
			break;
		}
	}




	return 0;
}