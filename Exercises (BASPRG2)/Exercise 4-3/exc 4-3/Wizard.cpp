#include "Wizard.h"
#include "Skill.h"

Wizard::Wizard(string name)
{
	mName = name;
	mMaxHp = 2000;
	mCurrentHp = mMaxHp;
	mSkill = new Skill;
	mSkill->Fireball();
}

Wizard::~Wizard()
{
}

string Wizard::GetName()
{
	return mName;
}

int Wizard::GetMaxHp()
{
	return mMaxHp;
}

int Wizard::getCurrentHp()
{
	return mCurrentHp;
}

int Wizard::setCurrentHp(int dmg)
{
	mCurrentHp -= dmg;
	if (mCurrentHp < 0) mCurrentHp = 0;
	return mCurrentHp;
}

void Wizard::UseSkill(Wizard* Target)
{
	Target->setCurrentHp(this->mSkill->GetDamage());
}

string Wizard::GetSkillName()
{
	return mSkill->GetName();
}
