#include "Skill.h"

Skill::Skill()
{
	mDamage = 0;
}

Skill::~Skill()
{
}

string Skill::GetName()
{
	return mName;
}

int Skill::GetDamage()
{
	int rng = rand()% 100 + mDamage;
	return rng;
}

void Skill::Fireball()
{
	mName = "fireball";
	mDamage = 200;
}
