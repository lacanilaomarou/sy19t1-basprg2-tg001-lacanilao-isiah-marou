#pragma once
#include <iostream>
#include <string>
#include "Skill.h"

using namespace std;

class Wizard
{
private:
	string mName;
	int mMaxHp;
	int mCurrentHp;
	Skill* mSkill;
public:
	Wizard(string name);
	~Wizard();
	string GetName();
	int GetMaxHp();
	int getCurrentHp();
	int setCurrentHp(int dmg);

	void UseSkill(Wizard* Target);
	string GetSkillName();
};