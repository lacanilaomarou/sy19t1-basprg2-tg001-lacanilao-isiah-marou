#pragma once
#include <iostream>
#include <string>
using namespace std;

class Skill
{
private:
	string mName;
	int mDamage;
public:
	Skill();
	~Skill();
	string GetName();
	int GetDamage();

	void Fireball();
};

