#include <iostream>
#include <string>

using namespace std;

class character
{
public:
	string charactername;
	int lvl;
	int exp;
	int exptonxtlvl;
	int currentHP;
	int maxHP;
	int currentMana;
	int maxMana;
	int strength, dexterity, vitality, magic, spirit, luck;
	int atk, atkpercent, def, defpercent, magicatk, magicdef, magicdefpercent;

	void status();
	void fight();
	void flee();
	void walk();
	void takequest();
	void buyitme();
	void die();
	void talk();
};

class skills
{
	string skillsname;
	int skillsmana; //how much it uses mana
	int skillsdmg;

	void useskill(int currentmana, int magic, int magicatk);
};

class wpn
{
	string wpnname;
	int wpndmg;

	void atk(int strength, int atk, int atkpercent);
};

class arm
{
	string armname;
	int armrating;
};

class acc
{
	string accname;
	
	void acceffect();
};