/// exercise 1-2 and 1-3

#include <iostream>
#include <string>

using namespace std;

void print(string items[]) // ex 1-2
{
	for (int i = 0; i < 8; i++)
	{
		cout << items[i] << " , ";
	}
	cout << endl << endl;
}

int cnt(string items[], string x) // 1-3
{
	int counter = 0;
	for (int i = 0; i < 8; i++)
	{
		if (x == items[i])
		{
			counter++;
		}
	}
	return counter;
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string x;

	print(items);		//1-2

	cout << "Enter string: ";		/// 1-3
	getline(cin, x);
	cout << cnt(items, x) << " " << x;


	return(0);
}
