#include <iostream>
#include <time.h>
#include <string>

using namespace std;

void print(int numbers[])
{
	for (int i = 0; i < 10; i++)
	{
		cout << numbers[i] << " , ";
	}
	cout << endl;
}

void fill(int *numbers) // 3-1
{
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}

}

int fillinternal() // ex 3-2
{
	int numbers[10];
	for (int i = 0; i < 10; i++)
	{
		numbers[i] = rand() % 100 + 1;
	}
	print(numbers);
	return *numbers;
}

int main()
{
	int numbers[10];

	//fill(numbers); //3-1


	*numbers = fillinternal(); // 3-2

	delete numbers;	// 3-3

}