#ifndef NODE_H
#define NODE_H

#include <string>
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

struct Cards
{
	string card;  //card name
	string beats; //what this card beats
	string loses; //what this card loses to
	Cards* next = NULL;
	Cards* prev = NULL;
};

#endif //NODE_H

struct matchresult
{
	bool roundresult; //who won this round
	int bet; //how much the player bets
	int reward; //how much the player wins or loses
};

const int targetmoney = 20000000; //20,000,000 fucking yen
const int reward = 100000;	//default reward;
const int mult = 5;	// slave multiplier;

int placebet(int distance); //placing bet
Cards draw(Cards* head, bool emperor); // draw cards
void printhand(Cards* head); // print player cards
void discard(Cards* head, int size, int choice); // delete choice card
int matchup(Cards* player, Cards* bot, int pchoice, int cchoice); // player card vs bot card
int playround(bool playeremperor, int round); // play round
int outcome(int bet, int& money, int& distance, int roundresult, bool playeremperor); // return total money gain or distance loss
void printmatchresult(matchresult* round, int number, int money, int distance); // show per round result after the match is over and match evaaluation (meh, good, bad)
void printside(bool playeremperor); // show which side are you
void play();  // function of all functions

int main()
{
	srand(time(NULL));
	char again;
	do
	{
		play(); // play the whole match

		cout << "\n\n\nDo you want to play again?(0 for no) ";
		cin >> again;
		system("cls");
		if (again == '0') break; //break if the player don't want to play another match anymore
	} while (true);

	cout << "THANK YOU FOR PLAYING!\n\n";

	return 0;
}

int placebet(int distance)
{
	cout << "Distance Remaining: " << distance << endl;
	int bet = 0;
	do
	{
		cout << "How far do you want to bet (in millimiters)? ";
		cin >> bet;
	} while (bet > distance);
	cout << endl;
	return bet;
}

void printside(bool playeremperor)
{
	cout << "Your Side: ";
	if (playeremperor == true)
	{
		cout << "Emperor" << endl;
	}

	else
	{
		cout << "Slave" << endl;
	}
}

Cards draw(Cards* head, bool emperor)
{
	Cards* current;

	for (int i = 0; i < 4; i++)
	{
		current = new Cards;
		current->card = "Citizen";
		current->beats = "Slaves";
		current->loses = "Emperor";
		current->next = head->next;
		head->next = current;
	}
	if (emperor == true)
	{
		current = new Cards;
		current->card = "Emperor";
		current->beats = "Citizen";
		current->loses = "Slaves";
		current->next = head->next;
		head->next = current;
	}
	else if (emperor == false)
	{
		current = new Cards;
		current->card = "Slaves";
		current->beats = "Emperor";
		current->loses = "Citizen";
		current->next = head->next;
		head->next = current;
	}
	return *head;
}

void printhand(Cards* head)
{
	Cards* current;
	current = head->next;
	int counter = 0;
	while (current)
	{
		counter++;
		cout << "[" << counter << "]" << " ";
		cout << current->card;
		current = current->next;
		cout << endl;
	}
}

int matchup(Cards* player, Cards* bot, int playerchoice, int botchoice)
{
	Cards* pcurrent; //temp for player
	pcurrent = player->next;
	for (int i = 0; i < playerchoice; i++)
	{
		pcurrent = pcurrent->next;
	}


	Cards* bcurrent; //temp for bot
	bcurrent = bot->next;
	for (int i = 0; i < botchoice; i++)
	{
		bcurrent = bcurrent->next;
	}

	cout << endl << "[YOU] " << pcurrent->card << "\n[BOT] " << bcurrent->card << endl;

	int result = -1; // -1 = draw, 0 = lose, 1 = win

	if (pcurrent->card == bcurrent->card)
	{
		cout << "\n\nDraw\n\n";
		result = -1;
		system("pause");
		system("cls");
	}

	if (pcurrent->beats == bcurrent->card)
	{
		cout << "\nYOU WIN\n";
		result = 1;
	}
	if (pcurrent->loses == bcurrent->card)
	{
		cout << "\nYou lose, you can't escape deaf\n";
		result = 0;
	}
	cout << endl;
	return result;
}

int outcome(int bet, int& money, int& distance, int roundresult, bool playeremperor)
{
	int temp;
	if (roundresult == 1) // player wins
	{
		if (playeremperor == true) // player on emperor
		{
			temp = bet * reward;
			cout << "You gained " << temp << " yen\n\n";
			money += temp;
			system("pause");
			system("cls");
			return temp;
		}
		else if (playeremperor == false) // player on slave side
		{
			temp = bet * reward * mult;
			cout << "You gained " << temp << " yen\n\n";
			money += temp;
			system("pause");
			system("cls");
			return temp;
		}
	}
	if (roundresult == 0) // cpu wins
	{
		distance -= bet;
		cout << "You lost " << bet << " millimeters\n\n";
		system("pause");
		system("cls");
		return distance;
	}

}

void discard(Cards* head, int size, int choice) // choice = what to delete
{

	Cards* prev = head;
	Cards* current = head->next;

	for (int i = 0; i < choice + 1; i++)
	{
		if (i == choice)
		{
			prev->next = current->next;
			delete current;
			break;
		}
		prev = current;
		current = current->next;
	}
}

int playround(bool playeremperor, int round)
{
	srand(time(NULL));


	int size = 5; //number of cards

	Cards* player = new Cards; //player's hand
	player->next = NULL; 
	Cards* bot = new Cards; //bot's hand
	bot->next = NULL;

	*player = draw(player, playeremperor);
	*bot = draw(bot, !playeremperor);

	int roundres; // round result


	do
	{
		cout << "========== Round " << round + 1 << " =======================\n";
		cout << "Player Cards:" << endl;
		printhand(player);
		cout << "========================================\n";

		int playerchooses;
		do
		{
			cout << "Pick a card to play: ";
			cin >> playerchooses;
		} while (playerchooses > size);


		int rng = rand() % size; //what the bot will choose

		roundres = matchup(player, bot, playerchooses - 1, rng);

		if (roundres != -1) break;
		else
		{
			discard(player, size, playerchooses - 1);
			discard(bot, size, rng);
		}

		size--;
	} while (true);

	delete player;
	delete bot;

	return roundres;
}

void printmatchresult(matchresult* round, int number, int money, int distance)
{
	/// FOR PRINT EVERY ROUND RESULT ///
	cout << "*************** " << "MATCH RESULT" << " ***************"<< endl;
	for (int i = 0; i < number; i++)
	{
		cout << "ROUND " << i + 1 << " =======================================" << endl;

		cout << "Winner: "; 

		if (round[i].roundresult == 0) cout << "Bot\n";
		if (round[i].roundresult == 1) cout << "You\n";

		cout << "Your bet: " << round[i].bet << endl;

		cout << "Outcome: ";

		if (round[i].roundresult == 0) cout << "Lost " << round[i].bet << "mm";
		if (round[i].roundresult == 1) cout << "Won " << round[i].reward << " yen";

		cout << endl << endl;
	}
	cout << "================================================" << endl << endl << endl;
	cout << "Total Money Gained: " << money << endl;
	cout << "Total Distnace Left: " << distance << endl;

	system("pause");
	system("cls");


	//// FOR EVALUATING THE ENDING ///
	if (distance <= 0) // bad ending
	{
		cout << "You lost your right ear, R.I.P in pieces";
	}
	else if (money >= targetmoney) // good ending
	{
		cout << "You can finnaly pay your student debt! Good Job!";
	}
	else if (money < targetmoney) // meh
	{
		cout << "You didn't lost your ear, but you didn't also met your target goal. Is it worth it? No, no it's not";
	}
}

void play()
{
	int distance = 30;

	int money = 0;

	matchresult* round = new matchresult[12];

	bool playeremperor = false; //who is emperor side
	int counter = 0; // to know how long the match lasted

	for (int i = 0; i < 12; i++)
	{
		if (i % 3 == 0) playeremperor = !playeremperor; //switching emperor side

		cout << "Round: " << i + 1 << "/12" << endl;

		cout << "Money: " << money << endl;

		printside(playeremperor); // print which side theplayer is on this round

		round[i].bet = placebet(distance); // get bet

		round[i].roundresult = playround(playeremperor, i); //play the round, return with the result

		round[i].reward = outcome(round[i].bet, money, distance, round[i].roundresult, playeremperor); //return how much was won or lost

		counter++; 

		if (distance <= 0) break; //stop the loop for bad ending
	}

	printmatchresult(round, counter, money, distance);
	delete[] round;
}